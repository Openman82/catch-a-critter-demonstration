﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace catchACritter
{
    class Timer
    {
        float timeRemaining = 0;
        Vector2 position = new Vector2(200, 10);
        SpriteFont font = null;
        bool running = false;

        // Delegates
        public delegate void  TimeUp(); // The delegates definition states what type of functions are allowed 
        public TimeUp ourTimerCallBack;

        public void LoadContent(ContentManager Content)
        {
            font = Content.Load<SpriteFont>("fonts/mainFont");

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the timer to the screen using the font variable
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(font, "Time remaining: " + timeInt.ToString(), position, Color.White);


      

        }
        public void Update(GameTime gameTime)
        {
            if (running)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeRemaining <= 0) // Where time runs out
                {
                    //Do something
                    running = false;
                    timeRemaining = 0;

                    if (ourTimerCallBack != null)
                        ourTimerCallBack();
                }
            }
        }
        public void StartTimer()
        {
            running = true;
        }
        public void SetTimer(float newTime)
        {
            
            timeRemaining = newTime;
        }

    }
}
