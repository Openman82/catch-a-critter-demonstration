﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace catchACritter
{

    class Button
    {
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        SoundEffect buttonSFX = null;
        bool visible = true;

        // Delegates
        public delegate void OnClick(); // The delegates definition states what type of functions are allowed 
        public OnClick ourButtonCallBack;

        public void LoadContent(ContentManager Content)
        {
            image = Content.Load<Texture2D>("graphics/button");
            buttonSFX = Content.Load<SoundEffect>("audio/buttonclick");
        }
        public void Draw(SpriteBatch spriteBatch)
        {


            if (visible == true)
            {
                
                spriteBatch.Draw(image, position, Color.White);
            }

        }
        public void input()
        {
            // Checks the current state of the mouse (Clicked/Not-Clicked/X & Y Positions)
            MouseState currentState = Mouse.GetState();

            // Get the bounding box for the critter
            Rectangle bounds = new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);


            // Checks if we're clicking the left mouse button and our mouse's position is within the the critter's boudning box.
            // It also checks if the critter is still alive. If not we wont be able to click on it.
            if (currentState.LeftButton == ButtonState.Pressed && bounds.Contains(currentState.X, currentState.Y) && visible == true)
            {
                buttonSFX.Play();
                visible = false;


                if (ourButtonCallBack != null)
                {
                    ourButtonCallBack();
                }
                


            }


        }
    }
}
