﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;


namespace catchACritter
{
    class Critter
    {   // Type Definiions -------------------
         public enum Species
            // A special type of integer with only specific values allowed, that have names
        {
            WALRUS, // 0
            TIGER,  // 1
            HIPPO,   // 2

            // --

            NUM
        }



        // ----------- Our Data(Can't be public) -------------
        Texture2D image = null;
        Vector2 position;
        bool alive = false; // (visible)
        Score scoreObject = null;
        int critterValue = 10;
        SoundEffect clickCritterSound;
        Species ourSpecies = Species.WALRUS ;
        // ----------- Behaviour(Can be public) ----------
        public Critter(Score newScore, Species newSpecies)
        {

            // Constructor. Called when the object is created.
            // No return type (special)
            // Helps decide how the object will be set up.
            // Can have arguments (such as newScore)
            // This one lets us have access to the game's score.
            scoreObject = newScore;
            ourSpecies = newSpecies;
        }
        //------------------------------------------------
        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/walrus");
        
            switch (ourSpecies)
            {
                case Species.WALRUS:
                    image = content.Load<Texture2D>("graphics/walrus");
                    critterValue = 5;
                    //Do something
                    break;
                case Species.HIPPO:
                    image = content.Load<Texture2D>("graphics/hippo");
                    critterValue = 15;
                    //Do something
                    break;
                case Species.TIGER:
                    image = content.Load<Texture2D>("graphics/panda");
                    critterValue = 10;
                    //Do something
                    break;
                default:
                    // This shouldn't happen
                    break;
            }




            clickCritterSound = content.Load<SoundEffect>("audio/buttonclick");

        }
        // ---------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {   
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
            
        }
        // ---------------------------------------------
        public void Spawn(GameWindow window)
        {
            //Upon spawning the critter set it to alive (is visible)
            alive = true;

            // Determine the space which the critter can spawn in (boundary)
            int positionYmin = 0;
            int positionXmin = 0;
            int positionYmax = window.ClientBounds.Height - image.Height;
            int positionXmax = window.ClientBounds.Width - image.Width;
            // Determine random position where the critter will spawn within the bounds
            Random rand = new Random();
            position.X = rand.Next(positionXmin, positionXmax);
            position.Y = rand.Next(positionYmin, positionYmax);
        }
        //------------------------------------------------
        public void Despawn()
        {
            // Upon despawn set the critter to not alive (invisible)
            alive = false;
        }
        //-----------------------------------------------
        public void Input()
        {
            // Checks the current state of the mouse (Clicked/Not-Clicked/X & Y Positions)
            MouseState currentState = Mouse.GetState();

            // Get the bounding box for the critter
            Rectangle critterBounds = new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);


            // Checks if we're clicking the left mouse button and our mouse's position is within the the critter's boudning box.
            // It also checks if the critter is still alive. If not we wont be able to click on it.
            if (currentState.LeftButton == ButtonState.Pressed && critterBounds.Contains(currentState.X, currentState.Y) && alive == true)
            {
                // We clicked the critter!

                // Upon clicking the critter we must despawn it.
                Despawn();
                clickCritterSound.Play();
                // Add to score TODO:
                scoreObject.AddScore(critterValue);
            }


        }
        
    }
}
