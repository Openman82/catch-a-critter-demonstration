﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace catchACritter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 10;
        Critter[] critterPool = new Critter[MAX_CRITTERS]; // a blank address for a critter array that will be created.
        Score OurScore; // a blank address for the player's score.
        Button ourButton = null; // blank address for the button.
        const float SPAWN_DELAY = 3f;
        float timeUntilNextSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;
        bool playing = false;
        Timer gameTimer = null;
        const float GAME_LENGTH = 30F;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            OurScore = new Score();
            OurScore.LoadContent(Content);
            ourButton = new Button();
            ourButton.LoadContent(Content);
            ourButton.ourButtonCallBack += StartGame; // Set the function that will be called when the button is clicked += means add to not become.
            gameTimer = new Timer();
            gameTimer.LoadContent(Content);
            gameTimer.SetTimer(GAME_LENGTH);
            gameTimer.StartTimer();
            gameTimer.ourTimerCallBack += EndGame;
            /* OurCritter = new Critter(OurScore);
            

             // Call the critter's function to load its content
             OurCritter.LoadContent(Content);
             OurCritter.Spawn(Window); */

            //Initialise random number generator for critter species
            Random rnd = new Random();


            // Create critter objects and load their content
            for (int i = 0; i < MAX_CRITTERS; ++i)
            {
                Critter.Species newSpecies = (Critter.Species)rnd.Next(0, (int)Critter.Species.NUM);


                Critter newCritter = new Critter(OurScore, newSpecies);
                newCritter.LoadContent(Content);

                critterPool[i] = newCritter;





            }


            IsMouseVisible = true; // Makes mouse visible.
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //OurCritter.Input();
            ourButton.input();

            foreach (Critter eachCritter in critterPool)
            {
                // Loop contents
                eachCritter.Input();
            }
            if (playing == true)
            {
                gameTimer.Update(gameTime);
                // Should a new critter spawn?
                timeUntilNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeUntilNextSpawn <= 0f)
                {
                    //Reset spawn timer
                    timeUntilNextSpawn = SPAWN_DELAY;
                    // Spawn a new critter

                    //TODO: Make sure critter is not alive before spawning
                    //Spawn next critter in list
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex; // TODO: Handle wrap index.
                }
                if (currentCritterIndex >= critterPool.Length)
                {
                    currentCritterIndex = 0;
                }
            }
          

            base.Update(gameTime);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            
            foreach (Critter eachCritter in critterPool)
            {
                // Loop contents
                eachCritter.Draw(spriteBatch);
            }
            ourButton.Draw(spriteBatch);

            OurScore.Draw(spriteBatch);
            gameTimer.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        void StartGame()
        {
            playing = true;
        }
        void EndGame()
        {
            playing = false;
            // Make Button visible
            //Remove critters on screen

            foreach(Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }

        }
    }
}
